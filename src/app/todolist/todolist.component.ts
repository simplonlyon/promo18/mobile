import { Component } from '@angular/core'

@Component({
  selector: 'ns-todo',
  templateUrl: './todolist.component.html',
})
export class TodoListComponent {
  task = '';
  todolist = ['task 1', 'task 2', 'task 3'];

  addTask() {
    this.todolist.push(this.task);
    this.task = '';
  }
}

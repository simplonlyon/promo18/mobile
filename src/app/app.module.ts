import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptFormsModule, NativeScriptHttpClientModule, NativeScriptModule } from '@nativescript/angular'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import {TodoListComponent} from './todolist/todolist.component'
import {AccelComponent} from './accel/accel.component'
import {CameraComponent} from './camera/camera.component'
import {HomeComponent} from './home/home.component'

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule
  ],
  declarations: [AppComponent, TodoListComponent,
  AccelComponent,
  CameraComponent,
  HomeComponent
  ],
  providers: [],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule { }

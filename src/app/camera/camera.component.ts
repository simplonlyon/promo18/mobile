import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core'
import * as camera from "@nativescript/camera";
import { ImageSource } from '@nativescript/core';
import {Tracked} from '../entities'


@Component({
  selector: 'ns-camera',
  templateUrl: './camera.component.html',
})
export class CameraComponent {
    picture:ImageSource;
    tracked:Tracked = {
      label: '',
      image: ''
    };

    constructor(private http:HttpClient) {}

  async goCamera() {
    await camera.requestPermissions();
    const image = await camera.takePicture({saveToGallery:false});
    this.picture = await ImageSource.fromAsset(image);
    
    
  }

  add() {
    this.tracked.image = this.picture.toBase64String('jpg');

    this.http.post<Tracked>('http://10.0.2.2:8080/api/tracked', this.tracked)
    .subscribe(data => console.log(data));
  }
}

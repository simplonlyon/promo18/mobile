export interface Tracked {
    id?:number;
    label:string;
    image:string;
}

export interface Location {
    id?:number;
    latitude:number;
    longitude:number;
    tracked?:Tracked;
    timestamp?:number;
}
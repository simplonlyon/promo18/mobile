import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core'
import {Location, Tracked} from '../entities'



@Component({
  selector: 'ns-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
    locations:Location[] = [];

    constructor(private http:HttpClient) {}

    ngOnInit() {
        this.http.get<Location[]>('http://10.0.2.2:8080/api/location')
        .subscribe(data => this.locations = data);
    }

  
}

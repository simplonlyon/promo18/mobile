import { NgModule } from '@angular/core'
import { Routes } from '@angular/router'
import { NativeScriptRouterModule } from '@nativescript/angular'
import { AccelComponent } from './accel/accel.component'
import { CameraComponent } from './camera/camera.component'
import { HomeComponent } from './home/home.component'
import { TodoListComponent } from './todolist/todolist.component'



const routes: Routes = [
  { path: '', pathMatch: 'full', component: TodoListComponent },
  { path: 'accel', component: AccelComponent },
  { path: 'camera', component: CameraComponent },
  { path: 'home', component: HomeComponent },

]

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule],
})
export class AppRoutingModule {}

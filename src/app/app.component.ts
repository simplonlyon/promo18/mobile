import { Component } from '@angular/core'

@Component({
  selector: 'ns-app',
  templateUrl: './app.component.html',
})
export class AppComponent {
  task = '';
  todolist = ['task 1', 'task 2', 'task 3'];

  addTask() {
    this.todolist.push(this.task);
    this.task = '';
  }
}

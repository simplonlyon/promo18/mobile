import { Component } from '@angular/core'
import * as  accelerometer from '@triniwiz/nativescript-accelerometer';

@Component({
  selector: 'ns-accel',
  templateUrl: './accel.component.html',
})
export class AccelComponent {
  position = {x:0,y:0,z:0};

  squarePos= {top: 100, left: 100}

  ngOnInit() {
    accelerometer.startAccelerometerUpdates(data => {
      this.position = data;
      this.squarePos.top -= data.y * 10;
      this.squarePos.left += data.x * 10;
    })
  }

  ngOnDestroy() {
    accelerometer.stopAccelerometerUpdates();
  }
}
